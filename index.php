<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
    <script src="./js/my-js.js"></script>
    <title>desafio-leo</title>
</head>
<body>
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                    <h4 class="modal-title">Título da mensagem</h4>
                </div>
                <div class="modal-body">
                    <p>Conteúdo da mensagem</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <?php
        if ((!isset($_COOKIE['visitada']))&&(empty($_COOKIE['visitada']))){
        ?>
            <script type="text/javascript">
            $("#myModal").modal('show');
            </script>
        <?php
            // cookie setado para expirar em 2 dias
            setcookie("visitada",  "sim", (time() + (2 * 24 * 3600)), "/");
        }
    ?>

    <header>
        <div class="container">

            <div class="pesquisa">
                <div class="img_logo">
                    <img src="img/logo_leo.png" alt="logo-leo">
                </div>
                <div class="nav_pesquisa">
                    <input class="input_pesquisa" type="text" class="txtPesquisa" placeholder="Pesquisar Cursos..."></input>
                </div>
                <div id="perfil">
                    <a href="#">
                        <img src="img/logo_perfil.png" alt="Profile image" id='profileImage'>
                    </a>
                    <h3 id="nome_perfil">
                        <span class="span_perfil" id="apresentacao_perfil">Seja bem vindo!</span>
                        <br>
                        <span id="nome_perfil">Mark Hunt</span>
                    </h3>
                    <span>
                        &#9660;
                    </span>
                </div>
            </div>
    </header>

    <div class="carousel">
        <div class="quadro_carousel">
            <h3>LOREM IPSUM</h3>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
				Morbi gravida nec urna sit amet vehicula. 
            </p>
            <div class="btn_quadro_carousel">
                <a href="">VER CURSO</a>
            </div>
        </div>
        <img id="img_carousel" src="img/img_carousel.jpg" alt="imagem carousel">
    </div>

    <div class="cursos">
        <div class="container cursos_container">
            <h1>MEUS CURSOS</h1>
            <hr>
            <div class="div_modal">
                
                <div class="navbar_cursos">

                    <div class="div_img">
                        <img class="img_cursos" src="img/img_cursos.png" alt="">
                        <h2>PELLENTESQUE MALESUADA</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Morbi gravida nec urna sit amet vehicula.
                        </p>
                        <a href="">VER CURSO</a>
                    </div>

                    <div class="div_img">
                        <img class="img_cursos" src="img/img_cursos.png" alt="">
                        <h2>PELLENTESQUE MALESUADA</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Morbi gravida nec urna sit amet vehicula.
                        </p>
                        <a href="">VER CURSO</a>
                    </div>

                    <div class="div_img">
                        <img class="img_cursos" src="img/img_cursos.png" alt="">
                        <h2>PELLENTESQUE MALESUADA</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Morbi gravida nec urna sit amet vehicula.
                        </p>
                        <a href="">VER CURSO</a>
                    </div>

                    <div class="div_img">
                        <img class="img_cursos" src="img/img_cursos.png" alt="">
                        <h2>PELLENTESQUE MALESUADA</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Morbi gravida nec urna sit amet vehicula.
                        </p>
                        <a href="">VER CURSO</a>
                    </div>

                    <div class="div_img">
                        <img class="img_cursos" src="img/img_cursos.png" alt="">
                        <h2>PELLENTESQUE MALESUADA</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Morbi gravida nec urna sit amet vehicula.
                        </p>
                        <a href="">VER CURSO</a>
                    </div>

                    <div class="div_img">
                        <img class="img_cursos" src="img/img_cursos.png" alt="">
                        <h2>PELLENTESQUE MALESUADA</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Morbi gravida nec urna sit amet vehicula.
                        </p>
                        <a href="">VER CURSO</a>
                    </div>

                    <div class="div_img">
                        <img class="img_cursos" src="img/img_cursos.png" alt="">
                        <h2>PELLENTESQUE MALESUADA</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Morbi gravida nec urna sit amet vehicula.
                        </p>
                        <a href="">VER CURSO</a>
                    </div>

                    <div class="curso_card add_curso">
                        <button onclick="openModal('modal-add-course')">
                            <img src="img/adicionar_curso.png" alt="">
                            <p>ADICIONAR<br>CURSO</p>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div id="footer">
            <div class="container" id="footer_container">
                <div id="footer_cont">	
                    <div id="footer_logo">
                        <img src="img/logo_footer.png" alt="logo footer">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
				        Morbi gravida nec urna sit amet vehicula. </p>
                    </div>
                    <div id="contato_footer">
                        <h4>// CONTATO</h4>
                        <a id="contato_footer_a" href="tel:+5521987653434">(21) 98765-3434</a>
                        <a id="contato_footer_a2" href="mailto:contato@leolearning">contato@leolearning</a>
                    </div>
                    <div id="midia_social_footer">
                        <h4>// REDES SOCIAIS</h4>
                        <div id="footer-social-media-itens">
                            <a href="#">
                                <img src="img/twitter.png" alt="Twitter">
                            </a>
                            <a href="#">
                                <img src="img/youtube.png" alt="Youtube">
                            </a>
                            <a href="#">
                                <img src="img/pinterest.png" alt="Pinterest">
                            </a>
                        </div>
                    </div>
                </div>		
            </div>
        </div>
        <div class="container" id="copyright_container">
            <div id="credits">
                Copyright 2021 - All rights reserved.
            </div>
        </div>
    </footer>  
    
    <div id="modal-add-course" class="modal">	
		<div class="modal-base" id="modal_base" >	
			<button class="close-modal" onclick="closeModal('modal-add-course')">
				&#10006;
			</button>
			<div class="modal-content" id="modal_content">	
				<div class="form_course">
					<div class="row">
						<div class="coluna">
							<label for="backgroundImageUrl">
								Imagem do curso
							</label>
							<input type="file" name="backgroundImageUrl" id="backgroundImageUrl" onchange="previewFile()">
						</div>
						<div class="row">
							<div class="coluna1">
								<label for="preview">
									Imagem
								</label>
								<img src="#" id="preview">
								<input type="hidden" name="background-img" id="background-img" value="">
							</div>
						</div>
					</div>
					<div class="row">
					  	<div class="coluna2">
							<label for="name">Título: </label>
					  	</div>
					  	<div class="coluna3">
							<input type="text" id="name" name="name" placeholder="Seu nome..">
					  	</div>
					</div>
					<div class="row">
						<div class="coluna2">
							<label for="description">Descrição:</label>
						</div>
						<div class="coluna3">
							<textarea id="description" name="description" placeholder="Descreva.." style="height:200px"></textarea>
						</div>
					</div>
					<div class="row">
					  	<button id='criar_curso' onclick="createCourse()">Criar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>